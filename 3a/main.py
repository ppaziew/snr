from data_generator import Data_generator
from keras.models import Sequential
from help_functions import *
import numpy as np
import cv2
import matplotlib.pyplot as plt
import os

from keras.models import Model
import scipy as sp



label_dict = get_label_dict(max_num_of_labels = 5)
train_data = get_train_data(label_dict)
test_data = get_test_data(label_dict)

random.shuffle(train_data)
number_of_classes = len(list(label_dict.keys()))
input_shape = cv2.imread(train_data[0][0]).shape

valid_data = train_data[:int(len(train_data) * 0.2)]
train_data = train_data[int(len(train_data) * 0.2):]

train_generator = Data_generator(train_data, 20,number_of_classes, data_augmentation=True)
valid_generator = Data_generator(valid_data, 20,number_of_classes, data_augmentation=True)




cam_model = get_cam_model(input_shape, number_of_classes)


#train and save cam_model
# model = get_model(input_shape, number_of_classes)
# for i in range(len(model.layers)):
#     if cam_model.layers[i].name == "global_average_pool":
#         break
#
#     model_layer_weights = model.layers[i].get_weights()
#     cam_model.layers[i].set_weights(model_layer_weights)
#
#
# model = cam_model
# model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
# model.fit_generator(generator=train_generator,
#                     validation_data=valid_generator,
#                     epochs=1,
#                     use_multiprocessing= True)
# model.save_weights('../models/vgg19_cam_model_5_classes.h5')


model = cam_model
model.load_weights(os.path.join('..', 'models', 'vgg19_cam_model_5_classes.h5',))

test_generator = Data_generator(test_data, len(test_data),number_of_classes, data_augmentation=False)
X_test, y_test = test_generator.__getitem__(0)

model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
model.evaluate(X_test, y_test)

gap_weights = model.layers[-1].get_weights()[0]
cam_model  = Model(inputs=model.input,outputs=(model.layers[-4].get_output_at(-1),model.layers[-1].output))
features,results = cam_model.predict(X_test)

for idx in range(features.shape[0]):
    features_for_one_img = features[idx,:,:,:]
    height_roomout = input_shape[0]/features_for_one_img.shape[0]
    width_roomout  = input_shape[1]/features_for_one_img.shape[1]

    cam_features = sp.ndimage.zoom(features_for_one_img, (height_roomout, width_roomout, 1), order=2)
    pred = np.argmax(results[idx])


    plt.figure(facecolor='white')
    cam_weights = gap_weights[:,pred]
    cam_output  = np.dot(cam_features,cam_weights)

    buf = 'Predicted Class = ' +str( pred )+ ', Probability = ' + str(results[idx][pred])

    plt.xlabel(buf)

    im = np.squeeze(X_test[idx])

    plt.imshow(im, alpha=0.5)
    plt.imshow(cam_output, cmap='jet', alpha=0.5)

    plt.show()

    input("hit enter for next...")











#
