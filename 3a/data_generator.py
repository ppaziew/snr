import keras
import cv2
import numpy as np
import albumentations as albu

from keras.applications.vgg19 import preprocess_input

class Data_generator(keras.utils.Sequence):
    def __init__(self, data, batch_size, number_of_classes, data_augmentation):
        self.data = data
        self.batch_size = batch_size
        self.img_shape = cv2.imread(data[0][0]).shape
        self.number_of_classes = number_of_classes
        self.data_augmentation = data_augmentation


    def __len__(self):
        return int(len(self.data) / self.batch_size)

    def __getitem__(self, index):
        data_indexes = list(range(index*self.batch_size,(index+1)*self.batch_size))

        X = np.zeros((self.batch_size, *self.img_shape), np.float32)
        # y = np.zeros((self.batch_size, self.number_of_classes ), np.int16)
        y = np.zeros((self.batch_size, ), np.int16)

        for i, data_index in enumerate(data_indexes):
            img = cv2.imread(self.data[data_index][0])

            # For displaying augmented images
            # x_pre = self.__augment_batch(img)
            # print("Zapisuję obrazek nr.{}".format(i))
            # cv2.imwrite("C:\\DEV\\PycharmProjects\\SNR_project\\snr\\X_NA_PREZENT\\obrazek{}.jpg".format(i), x_pre)

            img = preprocess_input(img)

            if self.data_augmentation == True:
                X[i, ] = self.__augment_batch(img)
            else:
                X[i,] = img

            y[i] = self.data[data_index][1]

        return X,y



    def __augment_batch(self, img_batch):
        img_batch = self.__random_transform(img_batch)

        return img_batch

    def __random_transform(self, img):
        h, w, c = img.shape
        composition = albu.Compose([albu.GaussNoise(var_limit=(10.0, 30.0), mean=0, always_apply=False, p=0.3),
                               albu.RandomCrop(height=round(h*0.97), width=round(w*0.97), always_apply=False, p=0.3),
                               albu.RandomBrightness(limit=0.2, always_apply=False, p=0.3),
                               albu.ElasticTransform(alpha=1, sigma=10, alpha_affine=1, interpolation=1, border_mode=4, value=None, mask_value=None, always_apply=False, approximate=False, p=0.3)
                              ])


        composed = composition(image=img)
        aug_img = composed['image']
        aug_img = cv2.resize(aug_img, (w,h))

        return aug_img