import sys
import numpy as np
import cv2
import glob
import random
import sklearn.metrics as sklm
import itertools
import os

from keras.applications.vgg19 import VGG19
from keras.preprocessing import image
from keras.layers import Input, Flatten, Dense,GlobalAveragePooling2D
from keras.models import Model
from keras.models import Sequential

import tensorflow as tf
from tensorflow.keras import Input, layers, Model



def get_label_dict(max_num_of_labels):
    if max_num_of_labels > 29 or max_num_of_labels < 2:
        print("Number of labels must be greater than 1 and less or equal 29")
        exit()

    all_labels = {
        'Z': 0, 'R': 1, 'U': 2, 'Y': 3, 'B': 4,
        'X': 5, 'S': 6, 'nothing': 7, 'E': 8, 'H': 9,
        'P': 10, 'M': 11, 'Q': 12, 'J': 13, 'N': 14,
        'I': 15, 'A': 16, 'F': 17, 'W': 18, 'K': 19,
        'C': 20, 'del': 21, 'T': 22, 'V': 23, 'space': 24,
        'D': 25, 'L': 26, 'O': 27, 'G': 28}
    label_dict = dict(itertools.islice(all_labels.items(), max_num_of_labels))

    return label_dict


def get_train_data(label_dict):
    train_data = []
    train_folders_paths = glob.glob(os.path.join('..', 'data', 'train', '*'))
    for train_folder_path in train_folders_paths:
        label = train_folder_path.split(os.sep)[-1]
        if label in label_dict:
            file_paths = glob.glob(train_folder_path + os.sep + "*")
            for file_path in file_paths:
                train_data.append((file_path, label_dict[label]))
    return train_data



def get_test_data(label_dict):
    test_data = []
    test_file_paths = glob.glob(os.path.join('..', 'data', 'test', '*'))
    for test_file_path in test_file_paths:
        label = test_file_path.split(os.sep)[-1][:-9]
        if label in label_dict:
            test_data.append((test_file_path, label_dict[label]))
    return test_data


def get_model(input_shape, number_of_classes ):
    model_vgg19_conv = VGG19(weights='imagenet', include_top=False, input_shape = input_shape)
    model = Sequential()

    for i in range(len(model_vgg19_conv.layers)):
        model.add(model_vgg19_conv.layers[i])
        if i != 0:
            model.layers[-1].trainable = False

    model.add(Flatten(name='flatten'))
    model.add(Dense(512, activation="relu", name='fc1'))
    model.add(Dense(number_of_classes, activation='softmax', name='predictions'))

    return model



def get_cam_model(input_shape, number_of_classes ):
    model_vgg19_conv = VGG19(weights='imagenet', include_top=False, input_shape = input_shape)
    model = Sequential()

    for i in range(len(model_vgg19_conv.layers)):
        model.add(model_vgg19_conv.layers[i])
        if i != 0:
            model.layers[-1].trainable = False

    model.add(GlobalAveragePooling2D(name = "global_average_pool"))
    model.add(Dense(512, activation="relu", name='fc1'))
    model.add(Dense(number_of_classes, activation='softmax', name='predictions'))

    return model
