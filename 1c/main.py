from data_generator import Data_generator
from help_functions import *

from train_and_save_models import train_and_save_models
from load_models import load_models
from test_models import test_models

import logging, os
logging.disable(logging.WARNING)
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"


label_dict = get_label_dict(max_num_of_labels = 5)
train_data = get_train_data(label_dict)
test = get_test_data(label_dict)


random.shuffle(train_data)
number_of_classes = len(list(label_dict.keys()))
input_shape = cv2.imread(train_data[0][0]).shape

valid_data = train_data[:int(len(train_data) * 0.2)]
train_data = train_data[int(len(train_data) * 0.2):]

train_generator = Data_generator(train_data, 100,number_of_classes, data_augmentation=True)
valid_generator = Data_generator(valid_data, 5,number_of_classes, data_augmentation=False)

model = get_model(input_shape, number_of_classes)

#train_and_save_models(model, train_generator,label_dict)


model.load_weights(os.path.join('..', 'models', 'vgg19_model_5_classes.h5'))
models = load_models()
print(models)

test_models(model, models, valid_generator)
