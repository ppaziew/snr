import glob
import pickle
import os

from sklearn.linear_model import SGDClassifier
import pickle

def load_models():
    linear_svm_classifiers = []
    squered_svm_classifiers = []
    exponential_svm_classifiers = []

    file_names = glob.glob(os.path.join('..', 'models', 'linear_svm_classifiers', '*'))
    for file_name in file_names:
        with open(file_name, 'rb') as fid:
            clf = pickle.load(fid)
        linear_svm_classifiers.append(clf)

    file_names = glob.glob(os.path.join('..', 'models', 'squered_svm_classifiers', '*'))
    for file_name in file_names:
        with open(file_name, 'rb') as fid:
            clf = pickle.load(fid)
        squered_svm_classifiers.append(clf)

    file_names = glob.glob(os.path.join('..', 'models', 'exponential_svm_classifiers', '*'))
    for file_name in file_names:
        with open(file_name, 'rb') as fid:
            clf = pickle.load(fid)
        exponential_svm_classifiers.append(clf)

    with open(os.path.join('..', 'models', 'linear_sdg_classifier.pkl'), 'rb') as fid:
        linear_sdg_classifier = pickle.load(fid)
    with open(os.path.join('..', 'models', 'squered_sdg_classifier.pkl'), 'rb') as fid:
        squered_sdg_classifier = pickle.load(fid)
    with open(os.path.join('..', 'models', 'exponetial_sdg_classifier.pkl'), 'rb') as fid:
        exponetial_sdg_classifier = pickle.load(fid)


    models = (linear_svm_classifiers,
           squered_svm_classifiers,
           exponential_svm_classifiers,
           linear_sdg_classifier,
           squered_sdg_classifier,
           exponetial_sdg_classifier)

    return models
