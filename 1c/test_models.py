from help_functions import *
import keras.backend as K
from sklearn.kernel_approximation import RBFSampler
from sklearn.kernel_approximation import AdditiveChi2Sampler
from sklearn.metrics import accuracy_score
import os

def test_models(model, models, valid_generator):
    linear_svm_classifiers = models[0]
    squered_svm_classifiers = models[1]
    exponential_svm_classifiers = models[2]
    linear_sdg_classifier = models[3]
    squered_sdg_classifier = models[4]
    exponetial_sdg_classifier = models[5]


    linear_svm_classifiers_predictions = []
    squered_svm_classifiers_predictions = []
    exponential_svm_classifiers_predictions = []
    linear_sdg_classifier_predictions = []
    squered_sdg_classifier_predictions =[]
    exponetial_sdg_classifier_predictions = []
    correct_predicts = []

    rbf_feature = RBFSampler(n_components=1000)
    squered_feature = AdditiveChi2Sampler()

    num_of_batches = valid_generator.__len__()
    num_of_batches = 10
    print("testing models:")
    for i in range(num_of_batches):
        print(str(i+1) + os.sep + str(num_of_batches))
        X , y = valid_generator.__getitem__(i)

        get_1st_layer_output = K.function([model.layers[0].get_input_at(-1)],
                                      [model.get_layer("flatten").get_output_at(-1)])
        features = get_1st_layer_output([X])[0]

        for j in range(features.shape[0]):
            features_to_predict = features[j:j+1, ]
            rbf_features_to_predict = rbf_feature.fit_transform(features_to_predict)
            squered_features_to_predict = squered_feature.fit_transform(features_to_predict)

            linear_svm_classifiers_prediction = combined_prediction(features_to_predict, linear_svm_classifiers)
            squered_svm_classifiers_prediction = combined_prediction(features_to_predict, squered_svm_classifiers)
            exponential_svm_classifiers_prediction = combined_prediction(features_to_predict, exponential_svm_classifiers)
            linear_sdg_classifier_prediction = linear_sdg_classifier.predict(features_to_predict)[0]
            exponetial_sdg_classifier_prediction = exponetial_sdg_classifier.predict(rbf_features_to_predict)[0]
            squered_sdg_classifier_prediction = squered_sdg_classifier.predict(squered_features_to_predict)[0]

            linear_svm_classifiers_predictions.append(linear_svm_classifiers_prediction)
            squered_svm_classifiers_predictions.append(squered_svm_classifiers_prediction)
            exponential_svm_classifiers_predictions.append(exponential_svm_classifiers_prediction)
            linear_sdg_classifier_predictions.append(linear_sdg_classifier_prediction)
            squered_sdg_classifier_predictions.append(squered_sdg_classifier_prediction)
            exponetial_sdg_classifier_predictions.append(exponetial_sdg_classifier_prediction)


        for j in range(y.shape[0]):
            correct_predicts.append(int(y[j]))



    linear_svm_classifiers_score = accuracy_score(correct_predicts, linear_svm_classifiers_predictions)
    squered_svm_classifiers_score = accuracy_score(correct_predicts, squered_svm_classifiers_predictions)
    exponential_svm_classifiers_score = accuracy_score(correct_predicts, exponential_svm_classifiers_predictions)
    linear_sdg_classifier_score = accuracy_score(correct_predicts, linear_sdg_classifier_predictions)
    squered_sdg_classifier_score = accuracy_score(correct_predicts, squered_sdg_classifier_predictions)
    exponetial_sdg_classifier_score = accuracy_score(correct_predicts, exponetial_sdg_classifier_predictions)

    print("linear_svm_classifiers_score: " + str(linear_svm_classifiers_score))
    print("squered_svm_classifiers_score: " + str(squered_svm_classifiers_score))
    print("exponential_svm_classifiers_score: " + str(exponential_svm_classifiers_score))
    print("linear_sdg_classifier_score: " + str(linear_sdg_classifier_score))
    print("squered_sdg_classifier_score: " + str(squered_sdg_classifier_score))
    print("exponetial_sdg_classifier_score: " + str(exponetial_sdg_classifier_score))
