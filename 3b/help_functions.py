import sys
import numpy as np
import cv2
import glob
import random
import sklearn.metrics as sklm
import scipy
import itertools
import os

from keras.preprocessing.image import load_img, save_img, img_to_array

from keras.applications.vgg19 import VGG19
from keras.applications.vgg19 import preprocess_input
from keras.applications import inception_v3

from keras.preprocessing import image
from keras.layers import Input, Flatten, Dense,GlobalAveragePooling2D
from keras.models import Model
from keras.models import Sequential

import tensorflow as tf
from tensorflow.keras import Input, layers, Model
from keras import backend as K


def get_label_dict(max_num_of_labels):
    if max_num_of_labels > 29 or max_num_of_labels < 2:
        print("Number of labels must be greater than 1 and less or equal 29")
        exit()

    all_labels = {
        'Z': 0, 'R': 1, 'U': 2, 'Y': 3, 'B': 4,
        'X': 5, 'S': 6, 'nothing': 7, 'E': 8, 'H': 9,
        'P': 10, 'M': 11, 'Q': 12, 'J': 13, 'N': 14,
        'I': 15, 'A': 16, 'F': 17, 'W': 18, 'K': 19,
        'C': 20, 'del': 21, 'T': 22, 'V': 23, 'space': 24,
        'D': 25, 'L': 26, 'O': 27, 'G': 28}
    label_dict = dict(itertools.islice(all_labels.items(), max_num_of_labels))

    return label_dict


def get_train_data(label_dict):
    train_data = []
    train_folders_paths = glob.glob(os.path.join('..', 'data', 'train', '*'))
    for train_folder_path in train_folders_paths:
        label = train_folder_path.split(os.sep)[-1]
        if label in label_dict:
            file_paths = glob.glob(train_folder_path + os.sep + "*")
            for file_path in file_paths:
                train_data.append((file_path, label_dict[label]))
    return train_data



def get_test_data(label_dict):
    test_data = []
    test_file_paths = glob.glob(os.path.join('..', 'data', 'test', '*'))
    for test_file_path in test_file_paths:
        label = test_file_path.split(os.sep)[-1][:-9]
        if label in label_dict:
            test_data.append((test_file_path, label_dict[label]))
    return test_data





def preprocess_image(image_path):
    # Util function to open, resize and format pictures
    # into appropriate tensors.
    img = load_img(image_path)
    img = img_to_array(img)
    img = np.expand_dims(img, axis=0)
    img = inception_v3.preprocess_input(img)
    return img




def deprocess_image(x):
    # Util function to convert a tensor into a valid image.
    if K.image_data_format() == 'channels_first':
        x = x.reshape((3, x.shape[2], x.shape[3]))
        x = x.transpose((1, 2, 0))
    else:
        x = x.reshape((x.shape[1], x.shape[2], 3))
    x /= 2.
    x += 0.5
    x *= 255.
    x = np.clip(x, 0, 255).astype('uint8')
    return x

def eval_loss_and_grads(x,fetch_loss_and_grads):
    outs = fetch_loss_and_grads([x])
    loss_value = outs[0]
    grad_values = outs[1]
    return loss_value, grad_values


def resize_img(img, size):
    img = np.copy(img)
    if K.image_data_format() == 'channels_first':
        factors = (1, 1,
                   float(size[0]) / img.shape[2],
                   float(size[1]) / img.shape[3])
    else:
        factors = (1,
                   float(size[0]) / img.shape[1],
                   float(size[1]) / img.shape[2],
                   1)
    return scipy.ndimage.zoom(img, factors, order=1)


def gradient_ascent(x, iterations, step, fetch_loss_and_grads, max_loss=None):
    for i in range(iterations):
        loss_value, grad_values = eval_loss_and_grads(x,fetch_loss_and_grads = fetch_loss_and_grads)
        if max_loss is not None and loss_value > max_loss:
            break
        print('..Loss value at', i, ':', loss_value)
        x += step * grad_values
    return x




def get_model(input_shape, number_of_classes ):
    model_vgg19_conv = VGG19(weights='imagenet', include_top=False, input_shape = input_shape)
    model = Sequential()

    for i in range(len(model_vgg19_conv.layers)):
        model.add(model_vgg19_conv.layers[i])
        if i != 0:
            model.layers[-1].trainable = False

    model.add(Flatten(name='flatten'))
    model.add(Dense(512, activation="relu", name='fc1'))
    model.add(Dense(number_of_classes, activation='softmax', name='predictions'))

    return model