Do poprawnego uruchomienia potrzebne jest:
scikit-learn version: 0.22.1

W glownym katalogu nalezy utworzyc folder data z folderami train i test.
W katalogu test powinny byc od razu zdjecia a w katalogu train podkatalogi kazdej z klas.

Zeby przyspieszyć trwnowanie testowałem kod tylko na 5 klasach.
żeby testowac na innej ilości trzeba zmienic argument funkcji
get_label_dict

1c.
Jako ze trenowanie modelow SVM na dziesiatkach tysiecy danych bardzo wielo
wymiarowych jest zabójcze dla pamieci podrecznej to zaimplementowalem
dwa sposoby obejscia tego problemu. Pierwszym jest uzycie SGDClassifier
który mozna trnować cżesciowo na częsci danych (online) i zastosowanie
odpowienidch transformat atrybutow tak zeby byly jadra wykladnicze i kwadrwatowe.
Drugim jest zbudowanie zbioru klasyfikatorow i zrobienie modelu "ensemble" i trenowanie
każdego na innej częsci danych a potem podejmowanie decyzji robiac "glosowanie".
