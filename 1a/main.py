from data_generator import Data_generator
from help_functions import *
import os


label_dict = get_label_dict(max_num_of_labels = 5)
train_data = get_train_data(label_dict)


random.shuffle(train_data)
number_of_classes = len(list(label_dict.keys()))
input_shape = cv2.imread(train_data[0][0]).shape


valid_data = train_data[:int(len(train_data) * 0.2)]
train_data = train_data[int(len(train_data) * 0.2):]

train_generator = Data_generator(train_data, 20,number_of_classes, data_augmentation=True)
valid_generator = Data_generator(valid_data, 20,number_of_classes, data_augmentation=True)

model = get_model(input_shape, number_of_classes)


# train and save model
# model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
# model.fit_generator(generator=train_generator,
#                     validation_data=valid_generator,
#                     epochs=1,
#                     use_multiprocessing= True)
#
# model.save_weights('my_model_weights2.h5')
model.load_weights(os.path.join('..', 'models', 'vgg19_model_5_classes.h5'))

#testing model
test_data = get_test_data(label_dict)
test_generator = Data_generator(test_data, len(test_data),number_of_classes, data_augmentation=True)
X_test, y_test = test_generator.__getitem__(0)
y_pred = model.predict(X_test)
print(y_test)
print([np.argmax(predicts) for predicts in y_pred])


# C:\Users\mk666\AppData\Local\Programs\Python\Python38\python.exe main.py

